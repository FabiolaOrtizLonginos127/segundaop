﻿/**
 * @file    arreglo_lista.h
 * @brief   Cabecera del fichero arreglo_lista.c
 * @par		Declaraciones 
 *			- classArregloBidi Definicion de la clase para crear un objeto que permita el control de arrays bidi float/int.
 *			- classAttArregloBidi Definicion de la estructura para los atributos de la clase classArregloBidi.
 *			- classLista Definicion de la clase para crear un objeto que permita el control de arrays uni float/int.
 *			- classAttLista Definición de la estructura para los atributos de la clase classLista. 
 * @author  ros_morales
 * @date    2020-04-17 
 */

#ifndef ARREGLO_LISTA_H
#define ARREGLO_LISTA_H
 
#include <stdio.h>
#include <malloc.h>

#define TAM_ELEMENTOS 32767

typedef struct LISTA classLista; // Clase Lista
typedef struct CLASS_ATT_LISTA classAttLista; // Atributos de la Clase Lista

// Definición de la clase Lista
struct LISTA{
	/**
 	* @brief   *atributos --> define los atributos de la clase LISTA
 	*/
	classAttLista *atributos;
	// ----> Para un array unidimensional de tipo float
	/**
 	* @brief   Define el número de elementos tendrá el array uni de tipo float, sí existe un error con el número de elementos retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   elementos valor de tipo int que define el número de elementos para el array
 	* @return  void
 	*/
 	// Define el número de elementos para un array bidi float
 	void (*setElementosListaFloat)(classLista *this, int elementos);
 	/**
 	* @brief   Se obtiene un array uni de tipo float (ejemplo: float *array_uni)
 	* @param   *this Objeto de tipo classLista
 	* @return  array_uni --> es el array uni de tipo float que se retornará
 	*/
 	// Se obtiene un array uni de tipo float
 	float *(*getListaFloat)(classLista *this);
 	/**
 	* @brief   Se define de una sola vez un array uni de tipo float
 	* @param   *this Objeto de tipo classLista
 	* @param   *array_uni puntero hacia un puntero de tipo float, array_uni es de entrada
	* @param   num_elementos valor de tipo int que define el número de elementos del arreglo float *array_uni
 	* @return  void
	* @todo    obtener de manera transparente el numero de elementos del array float *array_uni
 	*/
 	// Se define de una sola vez un array uni de tipo float (array_uni es de entrada)
 	void (*setListaFloat)(classLista *this, float *array_uni, int num_elementos);
 	/**
 	* @brief   Se agrega un valor float al array uni de tipo float, sí existe un error con el índice retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @param   valor elemento de tipo float que se ingresará al array unidimensional de tipo float
 	* @return  void
 	*/
 	// Se agrega un valor float al array uni de tipo float
 	void (*setValorListaFloat)(classLista *this, int indice, float valor);
 	/**
 	* @brief   Se obtiene un valor float del array uni de tipo float 
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @return  valor --> elemento de tipo float que se retornará, sí el indice esta mal se retorna -1.0
 	*/
 	// Se obtiene un valor float del array uni de tipo float 
 	float (*getValorListaFloat)(classLista *this, int indice);
 	/**
 	* @brief   Se libera la memoria ocupada de un array uni de tipo float 	
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
 	// Se libera la memoria de un array uni de tipo float 	
	void (*freeListaFloat)(classLista *this);
	/**
 	* @brief   Imprimir el array uni de tipo float
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
	// Imprimir el array uni de tipo float
	void (*printListaFloat)(classLista *this);
	/**
 	* @brief   Obtiene el número de elementos del array uni float
 	* @param   *this Objeto de tipo classLista
 	* @return  elementos --> valor de tipo int que retorna el número de elementos del array
 	*/
	// Obtener el numero de elementos del array uni float
	int (*getTotalElementosListaFloat)(classLista *this);
	// ----> Para un array unidimensional de tipo int
	/**
 	* @brief   Define el número de elementos tendrá el array uni de tipo int, sí existe un error con el número de elementos retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   elementos valor de tipo int que define el número de elementos para el array
 	* @return  void
 	*/
 	// Define el número de elementos para un array bidi int
 	void (*setElementosListaInt)(classLista *this, int elementos);
 	/**
 	* @brief   Se obtiene un array uni de tipo int (ejemplo: int *array_uni)
 	* @param   *this Objeto de tipo classLista
 	* @return  array_uni --> es el array uni de tipo int que se retornará
 	*/
 	// Se obtiene un array uni de tipo int
 	int *(*getListaInt)(classLista *this);
 	/**
 	* @brief   Se define de una sola vez un array uni de tipo int
 	* @param   *this Objeto de tipo classLista
 	* @param   *array_uni puntero hacia un puntero de tipo int, array_uni es de entrada
	* @param   num_elementos valor de tipo int que define el número de elementos del arreglo int *array_uni
 	* @return  void
	* @todo    obtener de manera transparente el numero de elementos del array int *array_uni
 	*/
 	// Se define de una sola vez un array uni de tipo int (array_uni es de entrada)
 	void (*setListaInt)(classLista *this, int *array_uni, int num_elementos);
 	/**
 	* @brief   Se agrega un valor float al array uni de tipo int, sí existe un error con el índice retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @param   valor elemento de tipo int que ingresará al array unidimensional de tipo int
 	* @return  void
 	*/
 	// Se agrega un valor int al array uni de tipo int
 	void (*setValorListaInt)(classLista *this, int indice, int valor);
 	/**
 	* @brief   Se obtiene un valor int del array uni de tipo int 
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @return  valor --> elemento de tipo int que se retornará, sí el indice esta mal se retorna -1
 	*/
 	// Se obtiene un valor int del array uni de tipo int 
 	int (*getValorListaInt)(classLista *this, int indice);
 	/**
 	* @brief   Se libera la memoria ocupada de un array uni de tipo int 	
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
 	// Se libera la memoria de un array uni de tipo int 	
	void (*freeListaInt)(classLista *this);
	/**
 	* @brief   Imprimir el array uni de tipo int
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
	// Imprimir el array uni de tipo int
	void (*printListaInt)(classLista *this);
	/**
 	* @brief   Obtiene el número de elementos del array uni int
 	* @param   *this Objeto de tipo classLista
 	* @return  elementos --> valor de tipo int que retorna el número de elementos del array
 	*/
	// Obtener el numero de elementos del array uni int
	int (*getTotalElementosListaInt)(classLista *this);
};

// Método para crear un objeto de tipo classLista
classLista *new_ObjLista();
// Método para eliminar un objeto de tipo classLista
void free_ObjLista(classLista *this);
#endif